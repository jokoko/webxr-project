# Client
The client allows the detection and virtual enrichment of billboards based on web technologies. 

## Install Client
* copy directory to webserver

## Start Client
* Browse to the webserver with your favored device

:exclamation: **Server must be running**

<hr><br><br>

## Additional Informations


## Classes
| class | description |
| ------ | ------ |
| js/ViewController | Handles displaying of AR advertisement content |
| js/StreamHandler | Get camera stream and catch frames from it | 
| js/ScriptLoader | Lazy load additional classes |
| js/BillboardDetection | Handles detection requests to the server | 
| js/widget/Widget | Content type super class | 
