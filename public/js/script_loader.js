/*
  Allows the loading of dependencies on runtime
*/


class ScriptLoader{

  /*
    takes: files to load
    callback: if loading is done
  */
  static load(urls, callback){

    let scriptsAvailable = true;
    let request = {pendingFiles: urls.length, callback: callback};
    this._requests.push(request);

    for(let i=0; i<urls.length; i++){

      if(!ScriptLoader._loadedFiles.includes(urls[i])){
        scriptsAvailable = false;
        ScriptLoader._loadedFiles.push(urls[i]);
        $.getScript(urls[i], this._scriptLoaded.bind(this, request));
      }else{
        this._scriptLoaded(request);
      }
    }


  }

  static init(cls){
    window[cls.name] = cls;
  }

  static getClass(name){
    return window[name];
  }

  static _scriptLoaded(request, data){
    request.pendingFiles--;

    if(request.pendingFiles == 0){
      if(request.callback){

      request.callback();
    }
    }
  }

}

ScriptLoader._loadedFiles = [];
ScriptLoader._requests = [];
