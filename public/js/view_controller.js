/*
  Controls the displaying of additional AR data for the billboards
*/

const API_PATH = "data/advertisements/"; //dir of billboard content files
const AVAILABLE_ADS_URL = "data/advertisements.json"; //available billboards file
const FADE_SPEED = 500;

class ViewController{

  constructor(sceneDom){
    ScriptLoader.load(['js/widget/widget.js']);
    this._notLoadedWidgets = 0;
    this._availableAdvertisements = [];
    this._scene = $('#'+sceneDom);
    this._currentAdvertisements = [];
    this._loadingAdvertisements = [];
    this._loadAvailableAdvertisements();
  }


  //load the list of available advertisements
  _loadAvailableAdvertisements(){
    let vc = this;
    fetch(AVAILABLE_ADS_URL).then(function(response) {
      if (response.ok){
        return response.json();
      }else{
        throw new Error('error');
      }
    }).then(function(advertisements) {
      for(let i=0; i < advertisements.length; i++){
        vc._availableAdvertisements.push(advertisements[i]);
      }
    });
  }

  /*
    load the data of a specific advertisement and display it
  */
  loadAdvertisement(id, position, size){

    if(id == null || this._advertisementIsLoading(id))
      return;

    let contentURL = this._getAdvertisementContentURL(id);

    if(contentURL == null)
      return;

    this._lockAdvertisement(id);

    //update position if visible
    if(this._advertisementIsVisible(id)){
      this._updateAdvertisementPosition(id, position, size);
      this._freeAdvertisement(id);
      return;
    }

    //load advertisement if not visible
    fetch(API_PATH + contentURL).then(function(response){
      if (response.ok){
        return response.json();
      }else{
        throw new Error('error');
      }
    }).then(function(advertisement){
      advertisement.label = id;
      this._displayAdvertisement(advertisement, size, position);
      this._freeAdvertisement(id);
    }.bind(this));
  }

  /*
    display a loaded advertisement
  */

  _displayAdvertisement(advertisement, size, position){
    advertisement.size = size;
    advertisement.position = position;
    advertisement._widgetContainer = this._createWidgetContainer(advertisement);
    this._currentAdvertisements.push(advertisement);

    this._loadView(advertisement, advertisement.views[0]);
  }

  // lock advertisement if changes happening
  _lockAdvertisement(id){
    this._loadingAdvertisements.push(id);
  }

  // free advertisement from locking
  _freeAdvertisement(id){
    this._loadingAdvertisements.splice(this._loadingAdvertisements.indexOf(id),1);
  }

  //update position of advertisement content container
  _updateAdvertisementPosition(id, position, size){
    let advertisement = this._getAdvertisement(id);
    advertisement.position = position;
    advertisement.size = size;
    this._updateWidgetContainer(this._getAdvertisement(id));
  }

  // hide an advertisement and unload content
  hide(advertisement){
    this._destroyWidgets(advertisement);
  }

  //handle actions (change view / web link)
  processAction(advertisement, action){
    if(action.type == "link"){
      window.open(action.value);
    }

    if(action.type == "change_view"){
      this._destroyWidgets(advertisement);
      this._loadView(advertisement, this._getView(advertisement, action.value), null);
    }
  }

  //get the content URL of an advertisement
  _getAdvertisementContentURL(label){
    for(let i=0; i<this._availableAdvertisements.length; i++){
      if(this._availableAdvertisements[i].label == label){
        return this._availableAdvertisements[i].data;
      }
    }
  }


  //true if advetisement is currently visible on display
  _advertisementIsVisible(label){
    for(let i=0; i<this._currentAdvertisements.length; i++){

      if(this._currentAdvertisements[i].label == label){
        return true;
      }
    }
  }

  //true if advetisement is currently loading
  _advertisementIsLoading(label){
    for(let i=0; i<this._loadingAdvertisements.length; i++){
      if(this._loadingAdvertisements[i] == label){
        return true;
      }
    }
  }


  //get a visible advertisement by specify id
  _getAdvertisement(id){
    for(let i=0; i<this._currentAdvertisements.length; i++){
      if(this._currentAdvertisements[i].label == id){
        return this._currentAdvertisements[i];
      }
    }
  }

  //load and display a specific view of an advertisement
  _loadView(advertisement, view){
    let lw = this.loadWidgets.bind(this);
    lw(view.widgets, advertisement.size, advertisement, (loadedWidgets)=>{
      advertisement._loadedWidgets = loadedWidgets;
      this._updateView(advertisement);
    });
  }

  //get an available view from an advertisement by its name
  _getView(advertiesement, title){
    for(let i=0; advertiesement.views.length; i++){
      if(advertiesement.views[i].title == title){
        return advertiesement.views[i];
      }
    }
    return null;
  }


  //load a list of widgets
  loadWidgets(widgets, advertisementSize, advertisement, callback){
    let neededClasses = [];

    for(let i=0; i<widgets.length; i++){
      neededClasses.push(Widget.getClassPath(widgets[i].type));
    }

    //lazy load specific widget classes
    ScriptLoader.load(neededClasses, ()=>{
      let loadedWidgets = [];
      for(let i=0; i<widgets.length; i++){
        let cls = Widget.getClass(widgets[i].type);
        if(cls)
        loadedWidgets.push(new cls(this, widgets[i],advertisement, advertisementSize));
      }
      callback(loadedWidgets);
    })

  }

  //unload all widgets of a given advertisement
  _destroyWidgets(advertisement){
    for(let i=0; i<advertisement._loadedWidgets.length; i++){
      advertisement._loadedWidgets[i]._onDestroy();
    }
    this._removeWidgetContainer(advertisement);
    advertisement._loadedWidgets = [];
  }

  //remove all visible elements of an advertisement
  _removeWidgetContainer(advertisement){
    while (advertisement._widgetContainer.firstChild) {
      advertisement._widgetContainer.removeChild(advertisement._widgetContainer.firstChild);
    }
  }

  //update position of a visible advertisement
  _updateWidgetContainer(advertisement){
    advertisement._widgetContainer.setAttribute('scale', advertisement.size.width + " "+ advertisement.size.height +" 1");
    advertisement._widgetContainer.setAttribute('position', advertisement.position.x -( (1-advertisement.size.width)/2) + " "+ -(advertisement.position.y -( (1-advertisement.size.height)/2))+" -0.5");
  }


  //create a new content container for a newly detected advertisement
  _createWidgetContainer(advertisement){

    let container = document.createElement('a-entity');
    container.setAttribute('color', "grey");

    let plane = document.createElement('a-plane');
    plane.setAttribute('material', 'opacity: 0.3; side:double; color:white;');
    plane.setAttribute('position', '0 0 0');

    container.appendChild(plane);
    this._scene.append(container);
    console.log(advertisement);
    container.setAttribute('scale', advertisement.size.width + " "+ advertisement.size.height +" 1");
    container.setAttribute('position', advertisement.position.x -( (1-advertisement.size.width)/2) + " "+ -(advertisement.position.y -( (1-advertisement.size.height)/2))+" -0.5");

    return container;

  }

  //draw loaded widgets to the screen
  _updateView(advertisement, callback){

    advertisement._notLoadedWidgets = advertisement._loadedWidgets.length;

    for(let i=0; i<advertisement._loadedWidgets.length; i++){
      advertisement._loadedWidgets[i].getView((view)=>{
        if(view != null){
          advertisement._widgetContainer.append(view);
        }else{
          console.log("view is null");
        }
      });
    }

  }

  //triggered if a widget is loaded
  _widgetLoaded(widget){
    this._notLoadedWidgets--;
  }

}

ScriptLoader.init(ViewController);
