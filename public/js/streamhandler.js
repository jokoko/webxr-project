/*
  The StreamHandler controls the video/camera playback and the fetching of snapshots.
*/

//set fix video width to save bandwith
let VIDEO_WIDTH = 416;

class StreamHandler{

  constructor(){
    this.onLoadedHandler = null;
    this.videoDom = document.querySelector("#video");
    this.canvas = document.querySelector('#canvas');
    this.videoDom.addEventListener('loadedmetadata', this._onMetaDataLoaded.bind(this), false);
    this.videoSize = {
      width: 0,
      height: 0
    };
    this.initialized = false;
  }


  /*
    Set the video source between the device camera and three example videos.
  */
  setVideoSource(type, onLoaded){
    this.onLoadedHandler = onLoaded;

    if(type == 'cam'){
      if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true })
          .then(function (stream) {
            this.videoDom.srcObject = stream;
          }.bind(this))
          .catch(function (error) {
            console.log("camera is not available", error);
          });
      }
    }

    if(type == 'amnesty-video')
      this._setVideo('data/videos/amnesty_only.mp4', 'video/mp4');

    if(type == 'aussenwerbung-degewo-video')
      this._setVideo('data/videos/außenwerbung+degewo.mp4', 'video/mp4');

    if(type == 'aussenwerbung-video')
      this._setVideo('data/videos/außenwerbung_only.mp4', 'video/mp4');

  }


  /*
    Get snapshot as img
  */
  getImage(onFinish){
    if(!this.initialized)
    return false;

    this._updateCanvasImage();
    return this.canvas.getContext('2d').getImageData(0, 0, this.videoSize.width, this.videoSize.height);
  }

  /*
    Get snapshot as blob (binary)
  */
  getBlob(onFinish){
    if(!this.initialized)
    return false;

    this._updateCanvasImage();

    canvas.toBlob( blob => {
      onFinish(blob);
    });
  }


  _updateCanvasImage(){
    let drawContext = this.canvas.getContext('2d');
    drawContext.fillRect(0, 0, this.videoSize.width, this.videoSize.height);
    drawContext.drawImage(this.videoDom, 0, 0, this.videoSize.width, this.videoSize.height);
  }


  _setVideo(src, type){
    let source = document.createElement('source');
    source.setAttribute('src', src);
    source.setAttribute('type', type);
    this.videoDom.appendChild(source);
  }

  _setCanvasSize(width, height){
    this.canvas.width = width;
    this.canvas.height = height;
  }

  _onMetaDataLoaded(){

    let drawContext = this.canvas.getContext('2d');
    this.videoRatio = this.videoDom.videoWidth / this.videoDom.videoHeight;

    //shrink video
    this.videoSize.width =  VIDEO_WIDTH;
    this.videoSize.height = parseInt(this.videoSize.width / this.videoRatio, 10);

    this._setCanvasSize(this.videoSize.width, this.videoSize.height);

    this.initialized = true;

    if(this.onLoadedHandler)
      this.onLoadedHandler();
  }

}
