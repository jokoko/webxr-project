/*
  A Button Widget 
*/

class ButtonWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
    this.constSize = 256;
    this.resFactor = 2;
  }

  _onLoaded(){}

  _onDestroy(){}

  _createView(data, callback){
    let entity = document.createElement('a-entity');

    entity.setAttribute('htmlembed', '');
    entity.className = "screen";
    this._getDOMObject((dom)=>{
      entity.appendChild(dom);
      entity.object3D.scale.set((1/this.resFactor)*1,(1/this.resFactor)*1,1);
      callback(entity);
    });
  }

  _getDOMObject(callback){


      let htmlContainer = document.createElement('div');
      htmlContainer.className = 'html-container';
      htmlContainer.setAttribute("style", "width:" + this.data.size.width*this.constSize*this.resFactor+"px;height:"+ this.data.size.height*this.constSize*this.resFactor+"px");

      let button = document.createElement('div');
      button.className = "widget-button";
      button.onclick=()=>{this._viewController.processAction(this._advertisement, this.data.action)};
      button.style.background = this.data.background;

      let container = document.createElement('div');
      container.className = 'widget-button-container';



      let img = document.createElement('img');
      img.src = this.data.icon ;

      container.appendChild(img);

      let p = document.createElement('p');
      p.innerHTML = this.data.title;

      container.appendChild(p);

      button.appendChild(container);

      htmlContainer.appendChild(button);


      callback(htmlContainer);
  }

}

ScriptLoader.init(ButtonWidget);
