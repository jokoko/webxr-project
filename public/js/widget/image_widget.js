/*
  A Widget to display an image
*/

class ImageWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
  }

  _onLoaded(){}

  _onDestroy(){
  }

  _createView(data, callback){

    let asset = document.getElementById('asset-' + data.source.id);

    if(asset == undefined){
      asset = document.createElement('img');
      asset.id = 'asset-'+data.source.id;
      asset.setAttribute('src', data.source.url);
      document.getElementById('assets').appendChild(asset);
    }

    document.getElementById('assets').append();
    let entity = document.createElement('a-plane');
    entity.setAttribute("scale", this.data.size.width + " " + this.data.size.height + " 1");
    entity.setAttribute('material', 'opacity: 0.95; src:#'+asset.id);
    callback(entity);
  }

}

ScriptLoader.init(ImageWidget);
