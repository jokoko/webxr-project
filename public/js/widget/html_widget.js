/*
  A Widget to display html content
*/

class HTMLWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
  }

  _onLoaded(){}

  _onDestroy(){}

  _createView(data, callback){
    let entity = document.createElement('a-entity');
    entity.setAttribute('htmlembed','');
    entity.innerHTML = data.html;
    callback(entity);
  }

}
