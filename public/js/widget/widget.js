/*
  Superclass of all widgets
*/

const WIDGET_PATH = "js/widget/";
const WIDGET_CLASS_SUFFIX = "Widget";
const WIDGET_FILE_SUFFIX = "_widget.js";

class Widget{

  /*
    Load a widget
  */

  constructor(viewController, data, ad, advertisementSize){
    this._loadingComponents = [];
    this._viewController = viewController;
    this._view = null;
    this._advertisement = ad;

    this.containerSize = advertisementSize;
    this.icon = 'data/images/widget.svg';
    this.data = data;
    this.data.position.z = this.data.position.z == undefined ? 0 : this.data.position.z;
  }

  /*
    get content to display on screen
  */
  getView(callback){
    if(this._view == null){
      this._loadView(()=>{
        callback(this._view);
      });
    }else{
      callback(this._view);
    }
  }

/*
  returns or creates the visible content of the widget
*/
  _loadView(callback){
    this._createView(this.data, (view)=>{
      if(view != null){

        let parentView = document.createElement('a-entity');
        parentView.appendChild(view);
        this._view = parentView;
        this._view.setAttribute('position',  -((1-this.data.size.width)/2)+this.data.position.x + " " + (((1-this.data.size.height)/2)-this.data.position.y) + " "+ this.data.position.z);
        $(this._view).ready(this._componentLoaded());
      }else{
        this._onLoaded();
      }

      if(callback)
        callback();
    });
  }

  /*
    create the actual visible content of the widget
  */
  _createView(data, callback){
    callback(null);
  }

  /*
    called when widget is loaded
  */
  _onLoaded(){

  }

  /*
    called when widget is unloaded
  */
  _onDestroy(){
    this._view = null;
  }


  _componentLoaded(){
    let id = this._loadingComponents.length;
    this._loadingComponents.push(id);
    return (((component)=>{
      var index = this._loadingComponents.indexOf(component);
      if(index > -1)
        this._loadingComponents.splice(index,1);

      if(this._loadingComponents.length == 0){
        this._onLoaded();
        this._viewController._widgetLoaded(this);
      }
    }).bind(this, id));
  }

  static getClassName(widget){
    widget += WIDGET_CLASS_SUFFIX;
    return widget.charAt(0).toUpperCase() + widget.slice(1);
  }

  static getClass(type){
    return ScriptLoader.getClass(Widget.getClassName(type));
  }

  static getClassPath(widget){
    return WIDGET_PATH + widget + WIDGET_FILE_SUFFIX;
  }

}
