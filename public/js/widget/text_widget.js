/*
  A Widget to display text
*/

class TextWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
  }

  _onLoaded(){}

  _onDestroy(){
  }

  _createView(data, callback){


    let out = document.createElement('a-entity');


    if(data.background){
      let bg = document.createElement('a-plane');
      bg.setAttribute('material', 'opacity:0.95;color:'+data.background);
      bg.setAttribute("scale", this.data.size.width + " " + this.data.size.height + " 1");

      out.appendChild(bg);
}
    let entity = document.createElement('a-text');
    entity.setAttribute('value', data.content);
    entity.setAttribute('color', data.color);
    entity.setAttribute('anchor', 'center');
    entity.setAttribute('wrap-count', data.wrapCount);

    entity.setAttribute('width', ""+(this.data.size.width-data.padding));
    entity.setAttribute('height', ""+( this.data.size.height-data.padding));
    entity.object3D.position.z = 0;

    out.appendChild(entity);




    callback(out);
  }

}

ScriptLoader.init(TextWidget);
