/*
  Specific widget declaration
*/

class DiscussWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
    this.icon = 'data/images/chat.svg';
  }

  _onLoaded(){}

  _onDestroy(){}

  _createView(data, callback){
    this._getHTML(data.source, (html)=>{
      let entity = document.createElement('a-entity');
      entity.setAttribute('htmlembed','');

      let htmlContainer = document.createElement('div');
      htmlContainer.className = 'html-container';
      htmlContainer.setAttribute("style","width:" + 400*this.size.width+"px;height:"+400*this.size.height+"px");
      htmlContainer.innerHTML = html;
      entity.appendChild(htmlContainer);
      callback(entity);
    });
  }

  _getHTML(discussionSource, callback){
    fetch(discussionSource).then(function(response) {
      if (response.ok)
        return response.json();
      else
      throw new Error('error');
    }).then(function(items) {
      let html = '';
      for(let i=0;i<items.length; i++){
        html+='<div class="discuss-item"><div class="discuss-avatar" style="background: url('+items[i].avatar+')"></div><div class="discuss-message">'+items[i].message+'</div></div>';
      }
      callback(html);
    });

  }

}

ScriptLoader.init(DiscussWidget);
