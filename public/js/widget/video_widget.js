/*
  A Widget to display videos
*/

class VideoWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
    this.icon = 'data/images/youtube.svg';
  }

  _onLoaded(){}

  _onDestroy(){
    console.log('asset-' +  this.data.source.id);
    document.getElementById('asset-' +  this.data.source.id).pause();
  }

  _createView(data, callback){

    let asset = document.getElementById('asset-' + data.source.id);

    if(asset == undefined){
      asset = document.createElement('video');
      asset.id = 'asset-'+data.source.id;

      asset.setAttribute('src', data.source.url);
      asset.setAttribute('webkit-playsinline', '');
      asset.setAttribute('playsinline', '');
      asset.onloadeddata = ()=>{
        asset.play();
      }
      document.getElementById('assets').appendChild(asset);
    }else{
      asset.play();
    }

    let entity = document.createElement('a-plane');
    entity.setAttribute("scale", this.data.size.width + " " + this.data.size.height + " 1");
    entity.setAttribute('material', 'opacity: 0.92; src:#'+asset.id);
    callback(entity);
  }

}

ScriptLoader.init(VideoWidget);
