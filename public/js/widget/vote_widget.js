/*
  A Widget to display a voting tool
*/

class VoteWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
    this.constSize = 254;
    this.resFactor = 4;
  }

  _onLoaded(){}

  _onDestroy(){}

  _createView(data, callback){
    let entity = document.createElement('a-entity');
    entity.setAttribute('htmlembed', '');
  //  entity.setAttribute('position',"");
    entity.className = "screen";
    this._getDOMObject((dom)=>{
      entity.appendChild(dom);
      entity.object3D.scale.set((1/this.resFactor)*1,(1/this.resFactor)*1,1);

      callback(entity);
    });
  }

  _getDOMObject(callback){
    //this._viewController.loadWidgets(this.data.widgets, this.size, (loadedWidgets)=>{

      let htmlContainer = document.createElement('div');
      htmlContainer.className = 'html-container';
      htmlContainer.setAttribute("style", "width:" + this.data.size.width*this.constSize*this.resFactor+"px;height:"+ this.data.size.height*this.constSize*this.resFactor+"px");

      let menu = document.createElement('div');
      menu.className = "widget-menu";

      if(this.data.background){
        menu.style.background = this.data.background;
      }

      let h1 = document.createElement('h1');
      h1.innerHTML = "Wie gefällt dir die Werbung?";
      h1.style.fontSize = "100px";


        menu.appendChild(h1);

      for(let i=0; i<5; i++){
        let button = document.createElement('img');
        button.className= "vote-button";
        button.src = "data/images/star.svg";
        menu.appendChild(button);
        htmlContainer.appendChild(menu);
      }

      console.log(htmlContainer);

      callback(htmlContainer);
  //  });
  }

}

ScriptLoader.init(VoteWidget);
