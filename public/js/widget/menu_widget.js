/*
  A Widget to display a menu
*/

class MenuWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
    this.constSize = 256;
    this.resFactor = 2;
  }

  _onLoaded(){}

  _onDestroy(){}

  _createView(data, callback){
    let entity = document.createElement('a-entity');
    entity.setAttribute('htmlembed', '');
  //  entity.setAttribute('position',"");
    entity.className = "screen";
    this._getDOMObject((dom)=>{
      entity.appendChild(dom);


      entity.object3D.scale.set((1/this.resFactor)*1,(1/this.resFactor)*1,1);
    //  entity.object3D.position.set(((1-this.data.size.width)/2)*this.containerSize.width , -this.data.size.height +0.25,
    //  0);


      callback(entity);
    });
  }

  _getDOMObject(callback){


      let htmlContainer = document.createElement('div');
      htmlContainer.className = 'html-container';
  //    htmlContainer.setAttribute("style", "width:" + (1040*this.data.size.width)+"px;height:"+(1040*this.data.size.height)+"px");


      htmlContainer.setAttribute("style", "width:" + this.data.size.width*this.constSize*this.resFactor+"px;height:"+ this.data.size.height*this.constSize*this.resFactor+"px");

      let menu = document.createElement('div');
      menu.className = "widget-menu";

      if(this.data.background){
        menu.style.background = this.data.background;
      }


      for(let i=0; i<this.data.items.length; i++){
        let div = document.createElement('div');
        let button = document.createElement('img');
        div.className= "widget-menu-button";

        button.src = this.data.items[i].icon ;
        button.onclick=()=>{this._viewController.processAction(this._advertisement, this.data.items[i].action)};
          div.appendChild(button);

        if(this.data.items[i].title){
          let p = document.createElement('p');
          p.innerHTML = this.data.items[i].title;
          div.appendChild(p);
        }else{
          button.className = "no-text"
        }


        menu.appendChild(div);
        htmlContainer.appendChild(menu);
      }

      console.log(htmlContainer);

      callback(htmlContainer);
  //  });
  }

}

ScriptLoader.init(MenuWidget);
