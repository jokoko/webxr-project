/*
  The BillboardDetection requests recognition of available billboards in an image.
*/

const PREDICTION_API = "http://127.0.0.1:5000/api/get_predictions";

class BillboardDetection{

  /*
    takes: image as binary
    returns: predictions of visible billboards
  */
  getPredictions(blob, onFinish) {
    this._getPredictions(blob).then(predictions => {
      onFinish(predictions);
    })
}

  /*
    server request for image recognition
  */
  _getPredictions(blob, onFinish){
    let data = new FormData();
    data.append("image", blob);

    let onF = onFinish;

    return new Promise((resolve, reject)=>{
      fetch(PREDICTION_API, {
        method: 'POST', // or 'PUT'
        body: data,
      }).then(async (response) => {
        resolve(await response.json());
      }).catch((error) => {
        console.error("Prediction API not reachable");
        this.initialized = false;
        reject(Error("Prediction API not reachable"));
      });
    });

  }
}
